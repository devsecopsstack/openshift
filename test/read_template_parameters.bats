#!/usr/bin/env bats
load "$BATS_LIBRARIES_DIR/bats-support/load.bash"
load "$BATS_LIBRARIES_DIR/bats-assert/load.bash"
load 'openshift-scripts'


function setup() {
  export TMP_DIR=$(mktemp -d)
  export OC_RESULT=()
  export DEV_TLS_CERTIFICATE="development
certificate"
  export PROD_TLS_CERTIFICATE="production
certificate"
  export OC_RESULT=("$(cat test/fixtures/oc_process_result.txt )")
}

function teardown() {
  rm -rf $TMP_DIR
  echo -
}


@test "read template parameters should read template and print only environment variables required by template" {
  # GIVEN
  export environment_name=test-app

  # WHEN
  run build_template_param_args test/fixtures/openshift.env test/fixtures/openshift-production.env < test/fixtures/template_with_parameters.yml

  # THEN
  assert_output '--param SOURCE_REPOSITORY_URL="the-repository-url" --param GITHUB_WEBHOOK_SECRET="prod-secret" --param REPLICA_COUNT="2" --param TLS_CERTIFICATE="$PROD_TLS_CERTIFICATE" --param environment_name="$environment_name" '
}

@test "read template parameters with only one dotenv file" {
  # GIVEN
  export environment_name=test-app

  # WHEN
  run build_template_param_args test/fixtures/openshift.env test/fixtures/openshift-integration.env < test/fixtures/template_with_parameters.yml

  # THEN
  assert_output '--param SOURCE_REPOSITORY_URL="the-repository-url" --param GITHUB_WEBHOOK_SECRET="dev-secret" --param REPLICA_COUNT="1" --param TLS_CERTIFICATE="$DEV_TLS_CERTIFICATE" --param environment_name="$environment_name" '
}

@test "read template parameters with no dotenv file" {
  # GIVEN
  export environment_name=test-app

  # WHEN
  run build_template_param_args test/fixtures/openshift-global.env test/fixtures/openshift-integration.env < test/fixtures/template_with_parameters.yml

  # THEN
  assert_output '--param environment_name="$environment_name" '
}


@test "read template parameters from environment should not evaluate parameters missing (environment or dotenv)" {
  # GIVEN

  # WHEN
  run build_template_param_args test/fixtures/openshift.env test/fixtures/openshift-production.env < test/fixtures/template_with_parameters.yml

  # THEN
  assert_output '--param SOURCE_REPOSITORY_URL="the-repository-url" --param GITHUB_WEBHOOK_SECRET="prod-secret" --param REPLICA_COUNT="2" --param TLS_CERTIFICATE="$PROD_TLS_CERTIFICATE" '
}
